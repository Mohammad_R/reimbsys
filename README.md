# Employee Reimbursment System (ERS)

## Project Description

The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.

## Technologies Used

* Apache Tomcat Server
* PostgreSQL
* Java Servlet
* JDBC
* Jackson Databind
* JUnit and Mockito
* Log4J
* JavaScript

## Features

* Supports different account types
* Employees can submit reimbursement requests
* Finance managers can Approve / Deny reimbursement requests
* Finance managers can Filter reimbursements by status


## Getting Started
   
- Be sure to have Apache Tomcat 9.0 and Java 8 runtime environment installed.
- Run the following command to clone the project.
`git clone https://gitlab.com/Mohammad_R/reimbsys.git`
