package com.example.eval.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.example.model.User;
import com.example.service.UserServiceImpl;

class UserServiceImplTests {
	
	private UserServiceImpl userService = new UserServiceImpl();


	
	@Test
	public void checkIfNonExistantUserVerificationReturnsNull() {
		assertEquals(null,userService.verifyLoginCredentials("safdafsdadfs", "asfdasfd23rfsda"));
	}
	
	@Test
	public void checkIfRealUserReturnsUser() {
		User realUser = userService.getUser("ham");
		User verfiyedUser = userService.verifyLoginCredentials("ham", "pass");
		assertEquals(realUser.getEmail(), verfiyedUser.getEmail());
		
	}
	
	@Test
	public void checkIfcorrectUsernameButWrongPassReturnsNull() {
		assertEquals(null,userService.verifyLoginCredentials("ham", "wrongpass"));
	}
	
	@Test
	public void checkIfgetUserReturnsNullIfInvalidUsername() {
		assertEquals(null,userService.getUser("faskjhdksadgfjjasgfbiuohwegrajbkgreasjrwagf"));
	}
	
	@Test
	public void checkIfgetUserReturnsNullIfInvaliduserID() {
		assertEquals(null,userService.getUser(768345278));
	}

}














