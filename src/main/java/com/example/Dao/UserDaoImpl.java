package com.example.Dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import com.example.model.User;
import com.example.model.UserRole;

public class UserDaoImpl implements UserDao{
	
	ReimbursementDBConnection reimbursementConnection;
	LookUpTablesDaoImpl lookUp;
	
	public UserDaoImpl() {
		reimbursementConnection = new ReimbursementDBConnection();
		lookUp = new LookUpTablesDaoImpl();
		
	}

	public User getByUsername(String username) {
		
		try(Connection con = reimbursementConnection.getDbConnection()){
//			System.out.println("attempting to get user with username");
			String sql ="select * from users where username=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			rs.next();
			UserRole role = lookUp.getUserRoleById(rs.getInt(7));
			return new User(rs.getInt(1), rs.getString(2), rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6), role);

			
		} catch(SQLException e) {
			e.printStackTrace();
			System.out.println("user not found");
		}
		return null;
	}

	


	public User getById(int id) {
		try(Connection con = reimbursementConnection.getDbConnection()){
//			System.out.println("attempting to get user with id");
			String sql ="select * from users where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			rs.next();
			UserRole role = lookUp.getUserRoleById(rs.getInt(7));
			return new User(rs.getInt(1), rs.getString(2), rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6), role);

			
		} catch(SQLException e) {
//			e.printStackTrace();
			System.out.println("user not found");
		}
		return null;
	}

//	here we will use our call-able function that we created in the database, the funciton is called insert_user
	public void insert(User user) {
		try(Connection con = reimbursementConnection.getDbConnection()){
			System.out.println("calling isnert_user to insert a new user into the database");
			String sql = "{? = call insert_user(?,?,?,?,?,?)}";
			CallableStatement cs = con.prepareCall(sql);
			
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setString(2, user.getUsername());
			cs.setString(3, user.getPassword());
			cs.setString(4, user.getFirstName());
			cs.setString(5, user.getLastName());
			cs.setString(6, user.getEmail());
			cs.setInt(7, user.getRole().getId());
			cs.execute();
			System.out.println("user successfully created");

			

			
		} catch(SQLException e) {
			e.printStackTrace();
			System.out.println("user creation failed");
		}
		
	}


}
