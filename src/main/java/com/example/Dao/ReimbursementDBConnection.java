package com.example.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ReimbursementDBConnection {

	
	private static final String URL = "jdbc:postgresql://database-1.cjrlh8nlohgj.us-west-1.rds.amazonaws.com:5432/reimbursementdb";
	private static final String username = "dbuser";
	private static final String password = "password";
	
	public Connection getDbConnection() throws SQLException{
		try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		return DriverManager.getConnection(URL, username, password);
	}
}
