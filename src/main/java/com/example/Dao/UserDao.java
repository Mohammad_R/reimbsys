package com.example.Dao;

import java.util.List;

import com.example.model.User;

public interface UserDao {

	public User getByUsername(String username);
	public User getById(int id);
	
	public void insert(User user);
//	public void edit(int id);
//	public void delete(int id);
	
//	public List<User> getAll();
	
}
