package com.example.Dao;

import com.example.model.ReimbursementStatus;
import com.example.model.ReimbursementType;
import com.example.model.UserRole;

public interface LookUpTableDao {

	public ReimbursementStatus getReimbStatus(String status);
	public ReimbursementType getReimbType(String type);
	public UserRole getUserRole(String role);
	
	public ReimbursementStatus getReimbStatusById(int id);
	public ReimbursementType getReimbTypeById(int id);
	public UserRole getUserRoleById(int id);
	
}
