package com.example.Dao;

import com.example.model.User;

import java.util.List;

import com.example.model.Reimbursement;


public interface ReimbursementDao {

	List<Reimbursement> getAll();
	List<Reimbursement> getBasedOnStatus(String status);
	
	void update(Reimbursement updatedTicket);
	
	
	
	void insert(Reimbursement ticket);
	
	List<Reimbursement> getAUsersTickets(User user);
	
	
}
