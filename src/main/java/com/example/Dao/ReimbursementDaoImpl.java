package com.example.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.example.model.Reimbursement;
import com.example.model.ReimbursementStatus;
import com.example.model.ReimbursementType;
import com.example.model.User;

public class ReimbursementDaoImpl implements ReimbursementDao {
	
	
	ReimbursementDBConnection reimbursementConnection;
	LookUpTablesDaoImpl lookUp;
	UserDaoImpl userDao;
	
	public ReimbursementDaoImpl() {
		reimbursementConnection = new ReimbursementDBConnection();
		lookUp = new LookUpTablesDaoImpl();
		userDao = new UserDaoImpl();
	}
	
	

	@Override
	public List<Reimbursement> getAll() {
		
		List<Reimbursement> cardList = new ArrayList<>();
		
		try(Connection con = reimbursementConnection.getDbConnection()){
			String sql = "select * from reimbursement";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			
			while(rs.next()) {
				User author = userDao.getById(rs.getInt(6));
				User resolver;
				try{
					resolver = userDao.getById(rs.getInt(7));
				}catch(SQLException e) {
					resolver = null;
				}
				ReimbursementStatus status = lookUp.getReimbStatusById(rs.getInt(8));
				ReimbursementType type = lookUp.getReimbTypeById(rs.getInt(9));
				
				cardList.add(new Reimbursement(rs.getInt(1), rs.getInt(2),rs.getTimestamp(3),rs.getTimestamp(4),rs.getString(5),author,resolver,status,type));
			}
			return cardList;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Failed to get all reimbursements");
		}
		
		return cardList;
	}

	@Override
	public List<Reimbursement> getBasedOnStatus(String status) {

		List<Reimbursement> cardList = new ArrayList<>();
		
		try(Connection con = reimbursementConnection.getDbConnection()){
			ReimbursementStatus ReimbStatus = lookUp.getReimbStatus(status);
			int statusId = ReimbStatus.getId();
			
			String sql = "select * from reimbursement where status_id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, statusId);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				User author = userDao.getById(rs.getInt(6));
				User resolver = userDao.getById(rs.getInt(7));
				ReimbursementStatus reimb_retrival_status = lookUp.getReimbStatusById(rs.getInt(8));
				ReimbursementType type = lookUp.getReimbTypeById(rs.getInt(9));
				
				cardList.add(new Reimbursement(rs.getInt(1), rs.getInt(2),rs.getTimestamp(3),rs.getTimestamp(4),rs.getString(5),author,resolver,reimb_retrival_status,type));
			}
			return cardList;
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("could not get list of reimbursement with status of "+status);
		}
		
		return cardList;
	}

	@Override
	public void update(Reimbursement updatedTicket) {
		
		try(Connection con = reimbursementConnection.getDbConnection()){
			
			String sql = "update reimbursement set amount=?, submitted=?, resolved=?, description=?, author=?, resolver=?, status_id=?, type_id=? where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, updatedTicket.getAmount());
			ps.setTimestamp(2, updatedTicket.getSubmitted());
			ps.setTimestamp(3, updatedTicket.getResolved());
			ps.setString(4, updatedTicket.getDescription());
			ps.setInt(5, updatedTicket.getAuthor().getId());
			ps.setInt(6, updatedTicket.getResolver().getId());
			ps.setInt(7, updatedTicket.getStatus().getId());
			ps.setInt(8, updatedTicket.getType().getId());
			ps.setInt(9, updatedTicket.getId());
			int result = ps.executeUpdate();
			System.out.println("a record has been successfully updated");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("failed to update reimbursemnt ticket status");
		}
		
	}

	@Override
	public void insert(Reimbursement ticket) {
		try(Connection con = reimbursementConnection.getDbConnection()){
			
			String sql = "insert into reimbursement(amount,submitted,resolved,description,author,resolver,status_id,type_id) values(?,?,?,?,?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, ticket.getAmount());
			ps.setTimestamp(2, ticket.getSubmitted());
			ps.setTimestamp(3, ticket.getResolved());
			ps.setString(4, ticket.getDescription());
			ps.setInt(5, ticket.getAuthor().getId());
			if(ticket.getResolved() == null) {
				ps.setNull(6, Types.NULL);
			}else {
				ps.setInt(6, ticket.getResolver().getId());
			}
			ps.setInt(7, ticket.getStatus().getId());
			ps.setInt(8, ticket.getType().getId());
			
			int affectedRows = ps.executeUpdate();
			if(affectedRows == 0)
				System.out.println("failed to insert new ticket into reimb table");
			else {
				System.out.println("successfully inserted new ticket into reimb database");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("failed to insert new ticket into reimb table :(");
		}
		
	}

	@Override
	public List<Reimbursement> getAUsersTickets(User user) {
		List<Reimbursement> cardList = new ArrayList<>();
		
		try(Connection con = reimbursementConnection.getDbConnection()){
			
			
			String sql = "select * from reimbursement where author=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, user.getId());
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				User author = userDao.getById(rs.getInt(6));
				User resolver = userDao.getById(rs.getInt(7));
				ReimbursementStatus reimb_retrival_status = lookUp.getReimbStatusById(rs.getInt(8));
				ReimbursementType type = lookUp.getReimbTypeById(rs.getInt(9));
				
				cardList.add(new Reimbursement(rs.getInt(1), rs.getInt(2),rs.getTimestamp(3),rs.getTimestamp(4),rs.getString(5),author,resolver,reimb_retrival_status,type));
			}
			return cardList;
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("could not get list of reimbursement with author of "+user.getUsername());
		}
		
		return cardList;
	}
	
	
	
	public Reimbursement getBasedOnId(int id) {
		try(Connection con = reimbursementConnection.getDbConnection()){
			
			String sql = "select * from reimbursement where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			rs.next();

			User author = userDao.getById(rs.getInt(6));
			User resolver = userDao.getById(rs.getInt(7));
			ReimbursementStatus reimb_retrival_status = lookUp.getReimbStatusById(rs.getInt(8));
			ReimbursementType type = lookUp.getReimbTypeById(rs.getInt(9));
			
			return new Reimbursement(rs.getInt(1), rs.getInt(2),rs.getTimestamp(3),rs.getTimestamp(4),rs.getString(5),author,resolver,reimb_retrival_status,type);

			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("could not get reimbursment by id");
		}
		
		return null;
	}
	
	
	
	

}
