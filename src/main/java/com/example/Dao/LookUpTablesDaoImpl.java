package com.example.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.model.ReimbursementStatus;
import com.example.model.ReimbursementType;
import com.example.model.UserRole;

public class LookUpTablesDaoImpl implements LookUpTableDao{
	
	
	ReimbursementDBConnection reimbursementConnection;
	
	public LookUpTablesDaoImpl() {
		reimbursementConnection = new ReimbursementDBConnection();
	}

	
	@Override
	public ReimbursementStatus getReimbStatus(String status) {
		try(Connection con = reimbursementConnection.getDbConnection()){
			String sql = "select * from reimbursement_status where status=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, status);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return new ReimbursementStatus(rs.getInt(1), rs.getString(2));
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("failed at getting record from lookup table");
		}
		
		return null;
	}

	@Override
	public ReimbursementType getReimbType(String type) {
		try(Connection con = reimbursementConnection.getDbConnection()){
			String sql = "select * from reimbursement_type where type=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, type);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return new ReimbursementType(rs.getInt(1), rs.getString(2));
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("failed at getting record from lookup table");
		}
		
		return null;
	}

	@Override
	public UserRole getUserRole(String role) {
		try(Connection con = reimbursementConnection.getDbConnection()){
			String sql = "select * from user_roles where role=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, role);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return new UserRole(rs.getInt(1), rs.getString(2));
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("failed at getting record from lookup table");
		}
		
		return null;
	}


	@Override
	public ReimbursementStatus getReimbStatusById(int id) {
		try(Connection con = reimbursementConnection.getDbConnection()){
			String sql = "select * from reimbursement_status where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return new ReimbursementStatus(rs.getInt(1), rs.getString(2));
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("failed at getting record from lookup table");
		}
		
		return null;
	}


	@Override
	public ReimbursementType getReimbTypeById(int id) {
		try(Connection con = reimbursementConnection.getDbConnection()){
			String sql = "select * from reimbursement_type where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return new ReimbursementType(rs.getInt(1), rs.getString(2));
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("failed at getting record from lookup table");
		}
		
		return null;
	}


	@Override
	public UserRole getUserRoleById(int id) {
		try(Connection con = reimbursementConnection.getDbConnection()){
			String sql = "select * from user_roles where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return new UserRole(rs.getInt(1), rs.getString(2));
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("failed at getting record from lookup table");
		}
		
		return null;
	}

}
