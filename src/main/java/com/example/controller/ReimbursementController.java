package com.example.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.Dao.LookUpTablesDaoImpl;
import com.example.model.Reimbursement;
import com.example.model.ReimbursementStatus;
import com.example.model.ReimbursementType;
import com.example.model.User;
import com.example.service.ReimbursementServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ReimbursementController {

	
	public ReimbursementServiceImpl rService;
	public LookUpTablesDaoImpl lookUp;
	
	public ReimbursementController(){
		rService = new ReimbursementServiceImpl();
		lookUp = new LookUpTablesDaoImpl();
	}
	
	public String createNewReimbursement(HttpServletRequest req) {
		System.out.println("attempting to create new ticket");
		if(!req.getMethod().equals("POST")) {
			return "html/index.html";
		}
		
		//make sure user is logged in
		User author = (User)req.getSession().getAttribute("currentUser");
		if(author == null) {
			return "html/index.html";
		}
		//make sure user is not admin
		
		if(author.getRole().getRole().equals("employee")) {
			
			int amount = Integer.parseInt(req.getParameter("formamount"));
			Date date = new Date();  
	        Timestamp submitted=new Timestamp(date.getTime());  
			String description = req.getParameter("formdescription");
			String stype = req.getParameter("formtype");
			ReimbursementType type = lookUp.getReimbType(stype.toLowerCase());
			if(type == null) {
				return "html/createNewTicket.html";
			}
			ReimbursementStatus status = lookUp.getReimbStatus("pending");
			
			Reimbursement ticket = new Reimbursement(amount,submitted,null,description,author,null,status,type);
			
			
			rService.addNewReimbursementRequest(author,ticket);
			
			if(author.getRole().getRole().equals("employee"))
				return "html/employeeHome.html";
			else {
				return "html/adminHome.html";
			}
		}
		else {
			return "html/denied.html";
		}
	
		
	}
	
	public String updateReimbursement(HttpServletRequest req) {
		System.out.println("hello from controller, updating the ticket now");
		if(!req.getMethod().equals("POST")) {
			return "html/index.html";
		}
		User resolver = (User)req.getSession().getAttribute("currentUser");
		if(resolver == null || resolver.getRole().getRole().equals("employee")) {
			return "html/index.html";
		}
		//how we get the data from the form and update the ticket
		int ticketId = Integer.parseInt(req.getParameter("formid"));
		Reimbursement ticket = rService.getById(ticketId);
		
		
		String stringStatus = req.getParameter("formstatus").toLowerCase();
		ReimbursementStatus status = lookUp.getReimbStatus(stringStatus);

 
        if (stringStatus.equals("approved"))
        	rService.approveReimbursement(ticket,resolver);
        else if (stringStatus.equals("denied"))
        	rService.denyReimbursement(ticket, resolver);
		
		return "html/adminHome.html";
	}
	
	public void returnAllTickets(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		
		User user = (User)req.getSession().getAttribute("currentUser");
		if(user.getRole().getRole().equals("admin")) {
			List<Reimbursement> cardList = rService.getAllReimbursements();
			res.setStatus(200);
			ObjectMapper mapper = new ObjectMapper();
			String response = mapper.writeValueAsString(cardList);
			res.setContentType("application/json");
			res.getWriter().write(response);
		}
		else {
			List<Reimbursement> cardList = null;
			res.setStatus(200);
			ObjectMapper mapper = new ObjectMapper();
			String response = mapper.writeValueAsString(cardList);
			res.setContentType("application/json");
			res.getWriter().write(response);
		}
		
		
		
	}
	
	public void returnAUsersTickets(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		
		//make sure user is logged in
		User user = (User)req.getSession().getAttribute("currentUser");
		if(user == null) {
			System.out.println("returning null reimb");
			List<Reimbursement> cardList = null;
			res.setStatus(200);
			ObjectMapper mapper = new ObjectMapper();
			String response = mapper.writeValueAsString(cardList);
			res.setContentType("application/json");
			res.getWriter().write(response);
		}
		else {
			System.out.println("returning list of reimb");
			List<Reimbursement> cardList = rService.getUsersReimbursements(user);
			res.setStatus(200);
			ObjectMapper mapper = new ObjectMapper();
			String response = mapper.writeValueAsString(cardList);
			res.setContentType("application/json");
			res.getWriter().write(response);
		}

	}
	
	public void returnApprovedTickets(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		
		User user = (User)req.getSession().getAttribute("currentUser");
		if(user.getRole().getRole().equals("admin")) {
		
			List<Reimbursement> cardList = rService.getApprovedReimbursements();
			res.setStatus(200);
			ObjectMapper mapper = new ObjectMapper();
			String response = mapper.writeValueAsString(cardList);
			res.setContentType("application/json");
			res.getWriter().write(response);
		}
		else {
			List<Reimbursement> cardList = null;
			res.setStatus(200);
			ObjectMapper mapper = new ObjectMapper();
			String response = mapper.writeValueAsString(cardList);
			res.setContentType("application/json");
			res.getWriter().write(response);
		}

	}
	
	public void returnDeniedTickets(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		
		
		User user = (User)req.getSession().getAttribute("currentUser");
		if(user.getRole().getRole().equals("admin")) {
			List<Reimbursement> cardList = rService.getDeniedReimbursements();
			res.setStatus(200);
			ObjectMapper mapper = new ObjectMapper();
			String response = mapper.writeValueAsString(cardList);
			res.setContentType("application/json");
			res.getWriter().write(response);
		}
		else {
			List<Reimbursement> cardList = null;
			res.setStatus(200);
			ObjectMapper mapper = new ObjectMapper();
			String response = mapper.writeValueAsString(cardList);
			res.setContentType("application/json");
			res.getWriter().write(response);
		}
		
		
	}
	
	public void returnPendingTickets(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		
		User user = (User)req.getSession().getAttribute("currentUser");
		if(user.getRole().getRole().equals("admin")) {
			System.out.println("inside pending reimbersment controller");
			List<Reimbursement> cardList = rService.getPendingReimbursements();
			res.setStatus(200);
			ObjectMapper mapper = new ObjectMapper();
			String response = mapper.writeValueAsString(cardList);
			res.setContentType("application/json");
			res.getWriter().write(response);
		}
		else {
			System.out.println("inside pending reimbersment controller");
			List<Reimbursement> cardList = null;
			res.setStatus(200);
			ObjectMapper mapper = new ObjectMapper();
			String response = mapper.writeValueAsString(cardList);
			res.setContentType("application/json");
			res.getWriter().write(response);
		}

	}
	
}
