package com.example.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.model.User;
import com.example.service.UserServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ControllerExample {

	static UserServiceImpl usrS = new UserServiceImpl();
	
	
	public static String login(HttpServletRequest req) {
		if(!req.getMethod().equals("POST")) {
			return "html/index.html";
		}
		
		
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		//verify user with username and password, im gon skip
		
		User user = usrS.getUser("ham");
		//now we got username nad pass we add it into the session and route to new url
		req.getSession().setAttribute("currentUser", user);
		return "html/employeeHome.html";
	}
	
	
	public static String logout(HttpServletRequest req) {
		
		req.getSession().invalidate();
		return "index.change";
	}
	
	
	public static void getSessionUser(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		//this just gets the user in the session and throws it back to the front end
		User user = (User)req.getSession().getAttribute("currentUser");
		res.getWriter().write(new ObjectMapper().writeValueAsString(user));

		PrintWriter pw = res.getWriter();
		ObjectMapper mapper = new ObjectMapper();
		String response = mapper.writeValueAsString(user);
		res.setContentType("application/json");
		pw.write(response);
	}
}
