package com.example.controller;

import javax.servlet.http.HttpServletRequest;

import com.example.model.User;
import com.example.service.UserServiceImpl;

public class UserController {

	public UserServiceImpl uService;

	public UserController(){
		uService = new UserServiceImpl();
	}
	
	public String login(HttpServletRequest req) {
		System.out.println("in user controller, attempting login");
		System.out.println(req.getMethod());
		if(!req.getMethod().equals("POST")) {
			return "html/index.html";
		}
		
		User user = uService.verifyLoginCredentials(req.getParameter("formusername"), req.getParameter("formpassword"));
		
		if(user == null) {
			return "wrongcreds.change";
		}else {
			req.getSession().setAttribute("currentUser", user);
			if(user.getRole().getRole().equals("employee"))
				return "html/employeeHome.html";
			else {
				return "html/adminHome.html";
			}
		}
		
		
	}
	
	public String logout(HttpServletRequest req) {
		req.getSession().invalidate();
		return "html/index.html";
	}
	
}
