package com.example.servlets;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.controller.ControllerExample;
import com.example.controller.ReimbursementController;
import com.example.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONDispatcherServlet {
	
	public ReimbursementController rController;
	
	JSONDispatcherServlet(){
		rController = new ReimbursementController();
	}
	
	public void process(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		
		switch(req.getRequestURI()) {
		
		 case "/ReimbursementSys/getsessionuser.json":
			 ControllerExample.getSessionUser(req, res);
			 break;
			 
		 case "/ReimbursementSys/getAUsersReimbursements.json":
			 rController.returnAUsersTickets(req,res);
			 break;
			 
		 case "/ReimbursementSys/getAllReimbursements.json":
			 rController.returnAllTickets(req,res);
			 break;
			 
		 case "/ReimbursementSys/getPendingReimbursements.json":
			 System.out.println("getting pending reimbursements for JS");
			 rController.returnPendingTickets(req,res);
			 break;
			 
		 case "/ReimbursementSys/getApprovedReimbursements.json":
			 rController.returnApprovedTickets(req,res);
			 break;
			 
		 case "/ReimbursementSys/getDeniedReimbursements.json":
			 rController.returnDeniedTickets(req,res);
			 break;
			 
			 
		default:
			res.getWriter().write(new ObjectMapper().writeValueAsString(null));
		
		}
	}

}
