package com.example.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JSONMasterServlet extends HttpServlet {

	public JSONDispatcherServlet JSONDispatcher;
	
	public JSONMasterServlet(){
		JSONDispatcher = new JSONDispatcherServlet();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		System.out.println("in JSON doGet");
		JSONDispatcher.process(req, res);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		System.out.println("in JSON doPost");
		JSONDispatcher.process(req, res);
	}
}
