package com.example.servlets;

import javax.servlet.http.HttpServletRequest;

import com.example.controller.ControllerExample;
import com.example.controller.ReimbursementController;
import com.example.controller.UserController;
import com.fasterxml.jackson.databind.ser.std.StdKeySerializers.Default;

public class RequestDispatcher {
	
	UserController uController;
	ReimbursementController rController;
	
	public RequestDispatcher() {
		uController = new UserController();
		rController = new ReimbursementController();
	}
	
	

	public String process(HttpServletRequest req) {
		
		switch(req.getRequestURI()) {
		
		//so here we create cases to handle different url's 
		case "/ReimbursementSys/login.change":
			System.out.println("in loing.change dispatcher");
			return uController.login(req);
			
			
		case "/ReimbursementSys/logout.change":
			System.out.println("in logout.change dispatcher");
			return uController.logout(req);
			
		case "/ReimbursementSys/submitNewTicket.change":
			System.out.println("creating new ticket");
			return rController.createNewReimbursement(req);
			
		case "/ReimbursementSys/editReimbursementTicket.change":
			System.out.println("creating new ticket");
			return rController.updateReimbursement(req);
			
		default:
			System.out.println("in default case");
			return "html/unsuccessfullLogin.html";
		}
		
	}
	
}
