package com.example.model;

import java.sql.Timestamp;

public class Reimbursement {

	private int id;
	private int amount;
	private Timestamp submitted;
	private Timestamp resolved;
	private String description;
	private User author;
	private User resolver;
	private ReimbursementStatus status;
	private ReimbursementType type;
	
	
	public Reimbursement(int id, int amount, Timestamp submitted, Timestamp resolved, String description, User author,
			User resolver, ReimbursementStatus status, ReimbursementType type) {
		super();
		this.id = id;
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.author = author;
		this.resolver = resolver;
		this.status = status;
		this.type = type;
	}
	
	public Reimbursement(int amount, Timestamp submitted, Timestamp resolved, String description, User author,
			User resolver, ReimbursementStatus status, ReimbursementType type) {
		super();
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.author = author;
		this.resolver = resolver;
		this.status = status;
		this.type = type;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getAmount() {
		return amount;
	}


	public void setAmount(int amount) {
		this.amount = amount;
	}


	public Timestamp getSubmitted() {
		return submitted;
	}


	public void setSubmitted(Timestamp submitted) {
		this.submitted = submitted;
	}


	public Timestamp getResolved() {
		return resolved;
	}


	public void setResolved(Timestamp resolved) {
		this.resolved = resolved;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public User getAuthor() {
		return author;
	}


	public void setAuthor(User author) {
		this.author = author;
	}


	public User getResolver() {
		return resolver;
	}


	public void setResolver(User resolver) {
		this.resolver = resolver;
	}


	public ReimbursementStatus getStatus() {
		return status;
	}


	public void setStatus(ReimbursementStatus status) {
		this.status = status;
	}


	public ReimbursementType getType() {
		return type;
	}


	public void setType(ReimbursementType type) {
		this.type = type;
	}


	@Override
	public String toString() {
		if(resolver == null) {
			return "Reimbursement [id=" + id + ", amount=" + amount + ", submitted=" + submitted + ", resolved=null" 
					+ ", description=" + description + ", author=" + author.toString() + ", resolver=null" + ", status="
					+ status.toString() + ", type=" + type.toString() + "]";
		}
		else {
		return "Reimbursement [id=" + id + ", amount=" + amount + ", submitted=" + submitted + ", resolved=" + resolved
				+ ", description=" + description + ", author=" + author.toString() + ", resolver=" + resolver.toString() + ", status="
				+ status.toString() + ", type=" + type.toString() + "]";
		}
	}
	
	
	
	

}
