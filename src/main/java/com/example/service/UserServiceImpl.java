package com.example.service;

import com.example.Dao.UserDaoImpl;
import com.example.model.User;

public class UserServiceImpl implements UserService {
	
	public UserDaoImpl userDao;
	
	public UserServiceImpl(){
		userDao = new UserDaoImpl();
	}

	@Override
	public User verifyLoginCredentials(String uname, String pass) {
		User user = userDao.getByUsername(uname);
		if(user != null) {
			if(user.getPassword().equals(pass)) {
				System.out.println("account verification successfull");
				return user;
			}
			else {
				System.out.println("failed login");
				return null;
			}
		}
		else {
			return null;
		}
		
		
	}

	@Override
	public boolean verifyAdminUser(User user) {
		if (user.getRole().getRole().equals("admin")){
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public User getUser(String username) {
		return userDao.getByUsername(username);
	}

	@Override
	public User getUser(int id) {
		return userDao.getById(id);
	}

}
