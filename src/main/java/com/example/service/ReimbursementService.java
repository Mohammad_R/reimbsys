package com.example.service;

import java.util.List;

import com.example.model.Reimbursement;
import com.example.model.User;

public interface ReimbursementService {
	
	public void addNewReimbursementRequest(User author,Reimbursement ticket);
	public List<Reimbursement> getUsersReimbursements(User user);
	
	public List<Reimbursement> getAllReimbursements();
	public List<Reimbursement> getApprovedReimbursements();
	public List<Reimbursement> getDeniedReimbursements();
	public List<Reimbursement> getPendingReimbursements();
	
	public void approveReimbursement(Reimbursement ticket, User ticketResolver);
	public void denyReimbursement(Reimbursement ticket, User ticketResolver);
	
	
}
