package com.example.service;

//import com.example.model.Reimbursement;
import com.example.model.User;

public interface UserService {

	public User verifyLoginCredentials(String uname, String pass);
	public boolean verifyAdminUser(User user);
	
	public User getUser(String username);
	public User getUser(int id);
	
	
	
}
