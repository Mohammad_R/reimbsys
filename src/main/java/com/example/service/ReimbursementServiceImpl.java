package com.example.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.example.LogClass;
import com.example.Dao.LookUpTablesDaoImpl;
import com.example.Dao.ReimbursementDaoImpl;
import com.example.model.Reimbursement;
import com.example.model.User;

public class ReimbursementServiceImpl implements ReimbursementService{

	public LogClass myLog;
	public ReimbursementDaoImpl reimbDao;
	public LookUpTablesDaoImpl lookUp;
	
	public ReimbursementServiceImpl() {
		myLog = new LogClass();
		reimbDao = new ReimbursementDaoImpl();
		lookUp = new LookUpTablesDaoImpl();
	}

	@Override
	public void addNewReimbursementRequest(User author, Reimbursement ticket) {
		reimbDao.insert(ticket);
		myLog.recordLog("added new ticket with id of "+ticket.getId()+" and author username of "+ticket.getAuthor().getUsername());
		
	}

	@Override
	public List<Reimbursement> getUsersReimbursements(User user) {
		return reimbDao.getAUsersTickets(user);
	}

	@Override
	public List<Reimbursement> getAllReimbursements() {
		return reimbDao.getAll();
	}

	@Override
	public List<Reimbursement> getApprovedReimbursements() {
		return reimbDao.getBasedOnStatus("approved");
	}

	@Override
	public List<Reimbursement> getDeniedReimbursements() {
		return reimbDao.getBasedOnStatus("denied");
	}

	@Override
	public List<Reimbursement> getPendingReimbursements() {
		return reimbDao.getBasedOnStatus("pending");
	}

	@Override
	public void approveReimbursement(Reimbursement ticket, User ticketResolver) {
		if (!ticket.getStatus().equals("approved")) {
			ticket.setStatus(lookUp.getReimbStatus("approved"));
			Date date = new Date();  
	        Timestamp ts=new Timestamp(date.getTime()); 
	        ticket.setResolved(ts);
	        ticket.setResolver(ticketResolver);
			reimbDao.update(ticket);
			myLog.recordLog("ticket with id of " +ticket.getId() +" has been approved by user with username of " + ticketResolver.getUsername());
		}else {
			System.out.println("ticket is already approved");
		}

		
	}

	@Override
	public void denyReimbursement(Reimbursement ticket, User ticketResolver) {
		if (!ticket.getStatus().equals("denied")) {
			ticket.setStatus(lookUp.getReimbStatus("denied"));
			Date date = new Date();  
	        Timestamp ts=new Timestamp(date.getTime()); 
	        ticket.setResolved(ts);
	        ticket.setResolver(ticketResolver);
			reimbDao.update(ticket);
			myLog.recordLog("ticket with id of " +ticket.getId() +" has been denied by user with username of " + ticketResolver.getUsername());
			
		}
		else {
			System.out.println("ticket is already denied");
		}
		
	}
	public Reimbursement getById(int id) {
		return reimbDao.getBasedOnId(id);
	}
	
}
