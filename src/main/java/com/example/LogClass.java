package com.example;

import org.apache.log4j.Logger;

public class LogClass {

	public final Logger log2 = Logger.getLogger(LogClass.class);
	
	public void recordLog(String myString) {
		log2.info(myString);
	}
	
}
