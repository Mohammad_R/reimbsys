package com.example;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.example.Dao.LookUpTableDao;
import com.example.Dao.LookUpTablesDaoImpl;
import com.example.Dao.ReimbursementDaoImpl;
import com.example.Dao.UserDaoImpl;
import com.example.model.Reimbursement;
import com.example.model.ReimbursementStatus;
import com.example.model.ReimbursementType;
import com.example.model.User;
import com.example.model.UserRole;

public class daoTests {
	
	public static void main(String[] args) {
		System.out.println("main method testing dao");
		
		UserDaoImpl userDao = new UserDaoImpl();
		LookUpTablesDaoImpl lookup = new LookUpTablesDaoImpl();
		
		//testing adding a new user
		UserRole myRole = lookup.getUserRole("employee");
		User user = new User("hamalaw2","password","moe","r","ham@xd.com", myRole);
//		userDao.insert(user);
		
		//testing getting user by id or username
		User userByName = userDao.getByUsername(user.getUsername());
		System.out.println("got user by his name");
		System.out.println(userByName.toString());
		
		User userById = userDao.getById(userByName.getId());
		System.out.println("got user by his id:");
		System.out.println(userById.toString());
		
		
		ReimbursementDaoImpl reimbDao = new ReimbursementDaoImpl();
		
		//testing getting all reimbursements
		List<Reimbursement> cardList = new ArrayList<>();
		
		cardList = reimbDao.getAll();
		System.out.println("all reimbursements:");
		for(Reimbursement x: cardList) {
			
			System.out.println(x.toString());
		}
		
		//test getting based on status
		List<Reimbursement> cardList2 = new ArrayList<>();
		
		cardList2 = reimbDao.getBasedOnStatus("pending");
		System.out.println("pending reimbursements:");
		for(Reimbursement x: cardList2) {
			System.out.println(x.toString());
		}
		
		//test inserting a new reimb
		Date date = new Date();  
        Timestamp ts=new Timestamp(date.getTime());
        
        ReimbursementStatus pendingStatus = lookup.getReimbStatus("pending");
        ReimbursementType foodType = lookup.getReimbType("food");
        
		Reimbursement myTicket = new Reimbursement(55555,ts,null,"testing ticket",userById,null,pendingStatus,foodType);
		reimbDao.insert(myTicket);
		
	
		

		
		//test getting a user's reimbursements
		List<Reimbursement> cardList3 = new ArrayList<>();
		
		cardList3 = reimbDao.getAUsersTickets(userById);
		System.out.println("testing getting tickets of a specific user:");
		for(Reimbursement x: cardList3) {
			System.out.println("testing updating a ticket");
			x.setResolver(userById);
			x.setResolved(ts);
			x.setStatus(lookup.getReimbStatus("approved"));
			reimbDao.update(x);
			System.out.println(x.toString());
		}
		
		List<Reimbursement> cardList4 = new ArrayList<>();
		
		cardList4 = reimbDao.getAUsersTickets(userById);
		System.out.println("testing getting tickets of a specific user:");
		for(Reimbursement x: cardList4) {
			System.out.println(x.toString());
		}
		
		
	}

}
