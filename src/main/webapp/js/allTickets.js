
window.onload = function(){

    getAllReimbursements();
}


function getAllReimbursements(){
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if(xhttp.readyState == 4 && xhttp.status == 200){
            let cardList = JSON.parse(xhttp.responseText);

            var tbody = document.getElementById("reimbTable");
            var tr;
            var th;
            var td;
            cardList.forEach(function(item){
                tr = document.createElement('tr');
                tbody.appendChild(tr);
                th = document.createElement('th');
                th.setAttribute("scope", "row");
                th.innerHTML = item.id;
                tr.appendChild(th);

                td = document.createElement('td');
                td.innerHTML = new Date(item.submitted);
                tr.appendChild(td);

                td = document.createElement('td');
                td.innerHTML = item.author.username;
                tr.appendChild(td);

                td = document.createElement('td');
                td.innerHTML = item.amount;
                tr.appendChild(td);

                td = document.createElement('td');
                td.innerHTML = item.description;
                tr.appendChild(td);

                td = document.createElement('td');
                td.innerHTML = item.status.status;
                tr.appendChild(td);

                td = document.createElement('td');
                td.innerHTML = item.type.type;
                tr.appendChild(td);

                if(item.resolver == null){
                    td = document.createElement('td');
                    td.innerHTML = "null";
                    tr.appendChild(td);

                    td = document.createElement('td');
                    td.innerHTML = "null";
                    tr.appendChild(td);
                }
                else{
                    td = document.createElement('td');
                    td.innerHTML = item.resolver.username;
                    tr.appendChild(td);

                    td = document.createElement('td');
                    td.innerHTML = new Date(item.resolved);
                    tr.appendChild(td);
                }


            });
        }
    }
    xhttp.open("GET", "http://localhost:8080/ReimbursementSys/getAllReimbursements.json")
    xhttp.send();
}






